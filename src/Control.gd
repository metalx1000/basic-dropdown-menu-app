extends Control
onready var namebox = get_node("WindowDialog/namebox")
onready var popup = get_node("WindowDialog")
onready var chooseBtn = get_node("Button")
onready var okBtn = get_node("WindowDialog/Button")
onready var label = get_node("Label")

func _ready():
	var names =  ["Kris","John","Jake"]
	for name in names:
		namebox.add_item(name)


#func _process(delta):
#	pass


func _on_choose_button_up():
	popup.visible = true


func _on_ok_button_up():
	popup.visible = false
	label.text = "You have selected " + namebox.get_item_text(namebox.selected)
